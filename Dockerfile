FROM centos:7

RUN yum -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
RUN yum -y install dropbear
RUN dropbearkey -t rsa -f /etc/dropbear/dropbear_rsa_host_key

RUN adduser term
RUN echo "term:term" | chpasswd

RUN chmod 640 /etc/dropbear/dropbear_rsa_host_key
CMD ["/usr/sbin/dropbear","-E","-F","-s","-j","-k","-p localhost:2222"]
